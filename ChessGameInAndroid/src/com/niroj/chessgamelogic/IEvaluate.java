package com.niroj.chessgamelogic;

import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;
import com.niroj.chessgamelogic.GameBoard.ChessSquareDescriptor;

public interface IEvaluate {
	
	public int Evaluate( ChessSquareDescriptor[][] chessBoard, ENUM_PLAYER player);
	public int Evaluate( ChessSquareDescriptor[][] chessBoard);

}
