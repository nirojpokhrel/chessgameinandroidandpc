package com.niroj.chessgamelogic;

import java.util.ArrayList;
import java.util.List;

import com.niroj.chessgamelogic.ChessGameConstants.ENUM_ITEM;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;
import com.niroj.chessgamelogic.GameBoard.ChessSquareDescriptor;

public abstract class AbstractGameSolver {
	protected ChessSquareDescriptor[][] mNextMoveSquare;
	protected IEvaluate mEvaluate = null;
	
	public AbstractGameSolver() {
	}
	public static class NextMoveDesc {
		public ENUM_ITEM mItemToMove;
		public int mStartX, mStartY, mEndX, mEndY;
	}
	
	public static class ChildSquare {
		public ChessSquareDescriptor[][] mChildGameSquare;
		public ChildSquare() {
			// TODO Auto-generated constructor stub
		}
		
		public ChildSquare( ChessSquareDescriptor[][] currentGame, int fromX, int fromY, int toX, int toY ) {
			mChildGameSquare = new ChessSquareDescriptor[currentGame.length][currentGame[0].length];
			for( int i=0; i<currentGame.length; i++ ) {
				for( int j=0; j<currentGame[0].length; j++ ) {
					ChessSquareDescriptor desc = new ChessSquareDescriptor();
					CopyChildDescriptor( desc, currentGame[i][j]);
					mChildGameSquare[i][j] = desc;
				}
			} 
			mChildGameSquare[toX][toY].mItem = currentGame[fromX][fromY].mItem;
			mChildGameSquare[toX][toY].mPlayerType = currentGame[fromX][fromY].mPlayerType;
			
			mChildGameSquare[fromX][fromY].mItem = ENUM_ITEM.ENUM_ITEM_EMPTY;
			mChildGameSquare[fromX][fromY].mPlayerType = ENUM_PLAYER.ENUM_PLAYER_NONE;
			
		}
		
		private void CopyChildDescriptor( ChessSquareDescriptor dst, ChessSquareDescriptor source ) {
			dst.mItem = source.mItem;
			dst.mPlayerType = source.mPlayerType;
		}
	};
	protected void CalculateDiffFromParent( ChessSquareDescriptor[][] parentPos, ChessSquareDescriptor[][] childPos ) {
		int diff = 0;
		for( int i=0; i<ChessGameConstants.FINAL_ROW_SIZE; i++ ) {
			for( int j=0; j<ChessGameConstants.FINAL_COLUMN_SIZE; j++ ) {
				if( parentPos[i][j].mPlayerType != childPos[i][j].mPlayerType ) {
					diff++;
				}
			}
		}
		System.out.println("Child Parent Diff "+diff);
		
	}

	public NextMoveDesc FindNextMove( ChessSquareDescriptor[][] currentGame, ENUM_PLAYER currentPlayer, int depth ) {
		//Find the solution
		if( mEvaluate == null ) {
			//A factory method for creating the functions
			mEvaluate = CreateEvaluateClass();
		}
		ImplementGameLogic(currentGame, currentPlayer, depth );
		return GenerateNextMove(currentGame);
	}
	
	protected abstract IEvaluate CreateEvaluateClass();

	private NextMoveDesc GenerateNextMove(ChessSquareDescriptor[][] currentGame) {
		// TODO Auto-generated method stub
		NextMoveDesc nextMove = new NextMoveDesc();
		for( int i=0; i<ChessGameConstants.FINAL_ROW_SIZE; i++ ) {
			for( int j=0; j<ChessGameConstants.FINAL_COLUMN_SIZE; j++ ) {
				if( currentGame[i][j].mPlayerType != mNextMoveSquare[i][j].mPlayerType ) {
					if( mNextMoveSquare[i] [j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_NONE ) {
						nextMove.mStartX = i;
						nextMove.mStartY = j;
					} else {
						nextMove.mEndX = i;
						nextMove.mEndY = j;
						nextMove.mItemToMove = currentGame[i][j].mItem;
					}
				}
			}
		}
		
		return nextMove;
	}
	
	protected List<ChildSquare> GenerateChildList( ChessSquareDescriptor[][] currentGame, ENUM_PLAYER currentPlayer ) {
		List<ChildSquare> childList = new ArrayList<ChildSquare>();
		
		for( int i=0; i<ChessGameConstants.FINAL_ROW_SIZE; i++ ) {
			for( int j=0; j<ChessGameConstants.FINAL_COLUMN_SIZE; j++ ) {
				if( currentGame[i][j].mPlayerType == currentPlayer ) {
					switch( currentGame[i][j].mItem ) {
					case ENUM_ITEM_PAWN:
						MovePawn( currentGame, i, j, childList, currentPlayer );
						break;
					case ENUM_ITEM_ROOK:
						MoveAllPositions( currentGame, ROOK_MOVEMENT, i, j, childList, currentPlayer );
						break;
					case ENUM_ITEM_KNIGHT:
						MoveSinglePosition( currentGame, KNIGHT_MOVEMENT, i, j, childList, currentPlayer );
						break;
					case ENUM_ITEM_BISHOP:
						MoveAllPositions( currentGame, BISHOP_MOVEMENT, i, j, childList, currentPlayer );
						break;
					case ENUM_ITEM_QUEEN:
						MoveAllPositions( currentGame, QUEEN_MOVEMENT, i, j, childList, currentPlayer );
						break;
					case ENUM_ITEM_KING:
						MoveSinglePosition( currentGame, KING_MOVEMENT, i, j, childList, currentPlayer );
						break;
						default:
							break;
					}
				}
			}
		}
		
		
		return childList;
	}
	
	private void MoveSinglePosition(ChessSquareDescriptor[][] currentGame,
			int[][] movementArray, int row, int col, List<ChildSquare> childList,
 ENUM_PLAYER currentPlayer) {
		// TODO Auto-generated method stub
		int newRow = -1, newCol = -1;
		for (int i = 0; i < movementArray.length; i++) {
			newRow = row + movementArray[i][0];
			newCol = col + movementArray[i][1];
			if (newRow >= 0
					&& newCol >= 0
					&& newRow < currentGame.length
					&& newCol < currentGame[0].length
					&& currentGame[newRow][newCol].mPlayerType != currentPlayer ) {
				ChildSquare childSquare = new ChildSquare(currentGame,
						row, col, newRow, newCol);
				childList.add(childSquare);
			}
		}
	}

	private void MoveAllPositions(ChessSquareDescriptor[][] currentGame, int[][] movementArray, int posX, int posY,
 List<ChildSquare> childList, ENUM_PLAYER currentPlayer) {
		// TODO Auto-generated method stub
		for (int i = 0; i < movementArray.length; i++) {
			int rowInc = movementArray[i][0];
			int colInc = movementArray[i][1];
			int rowPos = posX + rowInc;
			int colPos = posY + colInc;
			while (rowPos >= 0 && colPos >= 0 && rowPos < currentGame.length
					&& colPos < currentGame[0].length) {
				if (currentGame[rowPos][colPos].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_NONE) {
					ChildSquare childSquare = new ChildSquare(currentGame,
							posX, posY, rowPos, colPos);
					childList.add(childSquare);
				} else {
					if (currentGame[rowPos][colPos].mPlayerType != currentPlayer) {
						ChildSquare childSquare = new ChildSquare(currentGame,
								posX, posY, rowPos, colPos);
						childList.add(childSquare);
					}
					break;
				}
				rowPos = rowPos + rowInc;
				colPos = colPos + colInc;
			}
		}
	}

	private void MovePawn( ChessSquareDescriptor[][] currentGame, int row, int column, List<ChildSquare> childList,
			ENUM_PLAYER currentPlayer) {
		// TODO Auto-generated method stub
		if( currentPlayer == ENUM_PLAYER.ENUM_PLAYER_WHITE ){
			//Try moving upwards
			if( (row-1) < 0 )
				return;
			if( currentGame[row-1][column].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_NONE ) {
				ChildSquare childSquare = new ChildSquare( currentGame, row, column, row-1, column );
				childList.add(childSquare);
			}
			if( (column-1)>=0 && currentGame[row-1][column-1].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
				ChildSquare childSquare = new ChildSquare( currentGame, row, column, row-1, column-1 );
				childList.add(childSquare);
			}
			if( (column+1)< currentGame[0].length
					&& currentGame[row-1][column+1].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
				ChildSquare childSquare = new ChildSquare( currentGame, row, column, row-1, column+1 );
				childList.add(childSquare);				
			}
		
		} else if( currentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
			//Try moving upwards
			if( (row+1) >= currentGame.length )
				return;
			if( currentGame[row+1][column].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_NONE ) {
				ChildSquare childSquare = new ChildSquare( currentGame, row, column, row+1, column );
				childList.add(childSquare);
			}
			if( (column-1)>=0 && currentGame[row+1][column-1].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_WHITE ) {
				ChildSquare childSquare = new ChildSquare( currentGame, row, column, row+1, column-1 );
				childList.add(childSquare);
			}
			if( (column+1)< currentGame[0].length 
					&& currentGame[row+1][column+1].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_WHITE ) {
				ChildSquare childSquare = new ChildSquare( currentGame, row, column, row+1, column+1 );
				childList.add(childSquare);				
			}
		}
	}

	abstract protected void ImplementGameLogic(ChessSquareDescriptor[][] currentGame, ENUM_PLAYER currentPlayer, int depth );
	
	
	public static final int[][] BISHOP_MOVEMENT = { {1,1}, {-1,1}, {1,-1}, {-1,-1}};
	public static final int[][] ROOK_MOVEMENT = { {1,0}, {0,1}, {-1,0}, {0,-1}};
	public static final int[][] QUEEN_MOVEMENT = { {1,1}, {-1,1}, {1,-1}, {-1,-1},{1,0}, {0,1}, {-1,0}, {0,-1} }; //multiple of a number
	public static final int[][] KING_MOVEMENT = { {1,1}, {-1,1}, {1,-1}, {-1,-1},{1,0}, {0,1}, {-1,0}, {0,-1} }; //Just a single movement
	public static final int[][] KNIGHT_MOVEMENT = { {1,2}, {-1,2}, {1,-2}, {-1,-2}, {2,1}, {-2,1}, {2,-1}, {-2,-1} };
}
