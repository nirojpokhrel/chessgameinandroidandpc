package com.niroj.chessgamelogic;

import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;
import com.niroj.chessgamelogic.GameBoard.ChessSquareDescriptor;

public class SimpleEvaluateClass implements IEvaluate {
	
	public SimpleEvaluateClass() {
		// TODO Auto-generated constructor stub
		BLACK_PAWN_POSITION_VALUE = new int[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		BLACK_KNIGHT_POSITION_VALUE = new int[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		BLACK_BISHOP_POSITION_VALUE = new int[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		BLACK_ROOK_POSITION_VALUE = new int[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		BLACK_QUEEN_POSITION_VALUE = new int[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		BLACK_KING_POSITION_VALUE = new int[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		BLACK_KING_ENDING_POSITION_VALUE = new int[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		
		for( int i=0; i<ChessGameConstants.FINAL_ROW_SIZE; i++ ) {
			for( int j=0; j<ChessGameConstants.FINAL_COLUMN_SIZE; j++ ) {
				BLACK_PAWN_POSITION_VALUE[i][j] = WHITE_PAWN_POSITION_VALUE[ChessGameConstants.FINAL_ROW_SIZE-1-i][j];
				BLACK_KNIGHT_POSITION_VALUE[i][j] = WHITE_KNIGHT_POSITION_VALUE[ChessGameConstants.FINAL_ROW_SIZE-1-i][j];
				BLACK_BISHOP_POSITION_VALUE[i][j] = WHITE_BISHOP_POSITION_VALUE[ChessGameConstants.FINAL_ROW_SIZE-1-i][j];
				BLACK_ROOK_POSITION_VALUE[i][j] = WHITE_ROOK_POSITION_VALUE[ChessGameConstants.FINAL_ROW_SIZE-1-i][j];
				BLACK_QUEEN_POSITION_VALUE[i][j] = WHITE_QUEEN_POSITION_VALUE[ChessGameConstants.FINAL_ROW_SIZE-1-i][j];
				BLACK_KING_POSITION_VALUE[i][j] = WHITE_KING_POSITION_VALUE[ChessGameConstants.FINAL_ROW_SIZE-1-i][j];
				BLACK_KING_ENDING_POSITION_VALUE[i][j] = WHITE_KING_ENDING_POSITION_VALUE[ChessGameConstants.FINAL_ROW_SIZE-1-i][j];
			}
		}
	}
	@Override
	public int Evaluate(ChessSquareDescriptor[][] chessBoard, ENUM_PLAYER player) {
		int val = Evaluate( chessBoard);
		if( player == ENUM_PLAYER.ENUM_PLAYER_BLACK)
			return -val;
		else
			return val;
	}
	
	@Override
	public int Evaluate(ChessSquareDescriptor[][] chessBoard) {
		// TODO Auto-generated method stub
		int blackValue = 0, whiteValue = 0;
		for( int i=0; i<chessBoard.length; i++ ) {
			for( int j=0; j<chessBoard[0].length; j++ ) {
				switch(chessBoard[i][j].mItem) {
				case ENUM_ITEM_BISHOP:
					if( chessBoard[i][j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
						blackValue += BISHOP_NET_VALUE;
						blackValue += BLACK_BISHOP_POSITION_VALUE[i][j];
					} else {
						whiteValue += BISHOP_NET_VALUE;
						whiteValue += WHITE_BISHOP_POSITION_VALUE[i][j];
					}
					break;
				case ENUM_ITEM_KING:
					//Try to implement for the ending position as well where it is desirable to bring towards the middle
					if( chessBoard[i][j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
						blackValue += KING_NET_VALUE;
						blackValue += BLACK_KING_POSITION_VALUE[i][j];
					} else {
						whiteValue += KING_NET_VALUE;
						whiteValue += WHITE_KING_POSITION_VALUE[i][j];
					}
					break;
				case ENUM_ITEM_KNIGHT:
					if( chessBoard[i][j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
						blackValue += KNIGHT_NET_VALUE;
						blackValue += BLACK_KNIGHT_POSITION_VALUE[i][j];
					} else {
						whiteValue += KNIGHT_NET_VALUE;
						whiteValue += WHITE_KNIGHT_POSITION_VALUE[i][j];
					}
					break;
				case ENUM_ITEM_PAWN:
					if( chessBoard[i][j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
						blackValue += PAWN_NET_VALUE;
						blackValue += BLACK_PAWN_POSITION_VALUE[i][j];
					} else {
						whiteValue += PAWN_NET_VALUE;
						whiteValue += WHITE_PAWN_POSITION_VALUE[i][j];
					}
					break;
				case ENUM_ITEM_QUEEN:
					if( chessBoard[i][j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
						blackValue += QUEEN_NET_VALUE;
						blackValue += BLACK_QUEEN_POSITION_VALUE[i][j];
					} else {
						whiteValue += QUEEN_NET_VALUE;
						whiteValue += WHITE_QUEEN_POSITION_VALUE[i][j];
					}
					break;
				case ENUM_ITEM_ROOK:
					if( chessBoard[i][j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
						blackValue += ROOK_NET_VALUE;
						blackValue += BLACK_ROOK_POSITION_VALUE[i][j];
					} else {
						whiteValue += ROOK_NET_VALUE;
						whiteValue += WHITE_ROOK_POSITION_VALUE[i][j];
					}
					break;
				case ENUM_ITEM_EMPTY:
					if( chessBoard[i][j].mPlayerType == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
						
					} else {
						
					}
					break;
					default:
						return ChessGameConstants.NEGATIVE_INFINITY;
				}
			}
		}
		return (whiteValue - blackValue);
	}

	
	public static final int PAWN_NET_VALUE = 100;
	public static final int KNIGHT_NET_VALUE = 320;
	public static final int BISHOP_NET_VALUE = 330;
	public static final int ROOK_NET_VALUE = 500;
	public static final int QUEEN_NET_VALUE = 900;
	public static final int KING_NET_VALUE = 20000;
	
	public static final int[][] WHITE_PAWN_POSITION_VALUE = {
		{ 0,  0,  0,  0,  0,  0,  0,  0 },
		{ 50, 50, 50, 50, 50, 50, 50, 50 },
		{ 10, 10, 20, 30, 30, 20, 10, 10 },
		{ 5,  5, 10, 25, 25, 10,  5,  5 },
		{ 0,  0,  0, 20, 20,  0,  0,  0 },
		{ 5, -5,-10,  0,  0,-10, -5,  5 },
		{ 5, 10, 10,-20,-20, 10, 10,  5 },
		{ 0,  0,  0,  0,  0,  0,  0,  0 }
	};
	
	public static final int[][] WHITE_KNIGHT_POSITION_VALUE = {
		{ -50,-40,-30,-30,-30,-30,-40,-50},
		{ -40,-20,  0,  0,  0,  0,-20,-40},
		{ -30,  0, 10, 15, 15, 10,  0,-30},
		{ -30,  5, 15, 20, 20, 15,  5,-30},
		{ -30,  0, 15, 20, 20, 15,  0,-30},
		{ -30,  5, 10, 15, 15, 10,  5,-30},
		{ -40,-20,  0,  5,  5,  0,-20,-40},
		{ -50,-40,-30,-30,-30,-30,-40,-50},
	};
	
	public static final int[][] WHITE_BISHOP_POSITION_VALUE = {
		{ -20,-10,-10,-10,-10,-10,-10,-20},
		{ -10,  0,  0,  0,  0,  0,  0,-10},
		{ -10,  0,  5, 10, 10,  5,  0,-10},
		{ -10,  5,  5, 10, 10,  5,  5,-10},
		{ -10,  0, 10, 10, 10, 10,  0,-10},
		{ -10, 10, 10, 10, 10, 10, 10,-10},
		{ -10,  5,  0,  0,  0,  0,  5,-10},
		{ -20,-10,-10,-10,-10,-10,-10,-20},
	};
	
	public static final int[][] WHITE_ROOK_POSITION_VALUE = {
		  { 0,  0,  0,  0,  0,  0,  0,  0},
		  { 5, 10, 10, 10, 10, 10, 10,  5},
		  {-5,  0,  0,  0,  0,  0,  0, -5},
		  {-5,  0,  0,  0,  0,  0,  0, -5},
		  {-5,  0,  0,  0,  0,  0,  0, -5},
		  {-5,  0,  0,  0,  0,  0,  0, -5},
		  {-5,  0,  0,  0,  0,  0,  0, -5},
		  {0,  0,  0,  5,  5,  0,  0,  0}
	};
	
	public static final int[][] WHITE_QUEEN_POSITION_VALUE = {
		{-20,-10,-10, -5, -5,-10,-10,-20},
		{-10,  0,  0,  0,  0,  0,  0,-10},
		{-10,  0,  5,  5,  5,  5,  0,-10},
		{ -5,  0,  5,  5,  5,  5,  0, -5},
		{  0,  0,  5,  5,  5,  5,  0, -5},
		{-10,  5,  5,  5,  5,  5,  0,-10},
		{-10,  0,  5,  0,  0,  0,  0,-10},
		{-20,-10,-10, -5, -5,-10,-10,-20}
	};
	
	public static final int[][] WHITE_KING_POSITION_VALUE = {
		{-30,-40,-40,-50,-50,-40,-40,-30},
		{-30,-40,-40,-50,-50,-40,-40,-30},
		{-30,-40,-40,-50,-50,-40,-40,-30},
		{-30,-40,-40,-50,-50,-40,-40,-30},
		{-20,-30,-30,-40,-40,-30,-30,-20},
		{-10,-20,-20,-20,-20,-20,-20,-10},
		{ 20, 20,  0,  0,  0,  0, 20, 20},
		{ 20, 30, 10,  0,  0, 10, 30, 20}
	};
	
	public static final int[][] WHITE_KING_ENDING_POSITION_VALUE = {
		{-50,-40,-30,-20,-20,-30,-40,-50},
		{-30,-20,-10,  0,  0,-10,-20,-30},
		{-30,-10, 20, 30, 30, 20,-10,-30},
		{-30,-10, 30, 40, 40, 30,-10,-30},
		{-30,-10, 30, 40, 40, 30,-10,-30},
		{-30,-10, 20, 30, 30, 20,-10,-30},
		{-30,-30,  0,  0,  0,  0,-30,-30},
		{-50,-30,-30,-30,-30,-30,-30,-50}
	};
	
	public static int[][] BLACK_PAWN_POSITION_VALUE;
	public static int[][] BLACK_KNIGHT_POSITION_VALUE;
	public static int[][] BLACK_BISHOP_POSITION_VALUE;
	public static int[][] BLACK_ROOK_POSITION_VALUE;
	public static int[][] BLACK_QUEEN_POSITION_VALUE;
	public static int[][] BLACK_KING_POSITION_VALUE;
	public static int[][] BLACK_KING_ENDING_POSITION_VALUE;
}
