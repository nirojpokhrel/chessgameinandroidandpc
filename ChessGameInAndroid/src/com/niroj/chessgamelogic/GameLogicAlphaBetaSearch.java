package com.niroj.chessgamelogic;

import java.util.List;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;
import com.niroj.chessgamelogic.GameBoard.ChessSquareDescriptor;

public class GameLogicAlphaBetaSearch extends AbstractGameSolver {
	//private int alpha, beta;
	private int mInitialDepth;
	private ENUM_PLAYER mGamePlayingPlayer;
	
	public GameLogicAlphaBetaSearch() {
		// TODO Auto-generated constructor stub
		super();
	}

	@Override
	protected void ImplementGameLogic(ChessSquareDescriptor[][] currentGame, ENUM_PLAYER currentPlayer, int depth ) {
		// TODO Auto-generated method stub
		
		mNextMoveSquare = null;
		mInitialDepth = depth;
		mGamePlayingPlayer = currentPlayer;
		AlphaBetaSearch( currentGame, currentPlayer, depth, ChessGameConstants.NEGATIVE_INFINITY, ChessGameConstants.POSITIVE_INFINITY );
	}
	
	private int AlphaBetaSearch( ChessSquareDescriptor[][] currentGame, ENUM_PLAYER currentPlayer, int depth, int alpha, int beta ) {
		int retVal;
		
		if( depth == 0 ) { //It's mandatory to do Quiescence Search before returning 
			return mEvaluate.Evaluate(currentGame, mGamePlayingPlayer);
		}
		List<ChildSquare> childList = GenerateChildList(currentGame, currentPlayer);
		if( mGamePlayingPlayer == currentPlayer ) {
			retVal = ChessGameConstants.NEGATIVE_INFINITY;
			for( ChildSquare child: childList ) {
				retVal = Math.max(retVal, AlphaBetaSearch(child.mChildGameSquare, GameBoard.getOpponent(currentPlayer), depth-1, alpha, beta ) );
				//Make sure the alpha is stored properly so that it gets reflected 
				if( depth ==  mInitialDepth ) {
					if( mNextMoveSquare == null ) {
						mNextMoveSquare = child.mChildGameSquare;
					} else if( alpha < retVal ) {
						alpha = retVal;
						mNextMoveSquare = child.mChildGameSquare;
					}
				} else
					alpha = Math.max(alpha, retVal);
				
				if( beta <= alpha || alpha > 10000 ) { //if alpha is > 20000 is the game over scenario so no need to search
					break; //Beta cut-off
				}
				
			}
			return retVal;
			
		} else {
			retVal = ChessGameConstants.POSITIVE_INFINITY;
			for( ChildSquare child: childList ) {
				retVal = Math.min(retVal, AlphaBetaSearch(child.mChildGameSquare, GameBoard.getOpponent(currentPlayer), depth-1, alpha, beta ) );
				beta = Math.min(retVal, beta);
				if( beta <= alpha || beta < -10000 ) { // if beta is < -20000 game is over
					break; //alpha cut-off
				}
			}
			
			return retVal;
		}
	}
	
	private int QuiescenceSearch( ) {
		
		
		return 0;
	}

	@Override
	protected IEvaluate CreateEvaluateClass() {
		// TODO Auto-generated method stub
		return new SimpleEvaluateClass();
	}

}
