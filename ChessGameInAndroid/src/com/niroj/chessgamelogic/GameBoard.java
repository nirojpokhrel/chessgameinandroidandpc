package com.niroj.chessgamelogic;

import com.niroj.chessgamelogic.AbstractGameSolver.NextMoveDesc;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_ITEM;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;

public class GameBoard {
	
	public ChessSquareDescriptor[][] mGameSquare;
	//IEvaluate mTestEvaluateFunc;
	private AbstractGameSolver mGameSolver;
	
	public static class ChessSquareDescriptor {
		public ENUM_ITEM mItem;
		public ENUM_PLAYER mPlayerType;
	};
	public static ENUM_ITEM[] mInitialItems = { ENUM_ITEM.ENUM_ITEM_ROOK,
			ENUM_ITEM.ENUM_ITEM_KNIGHT, ENUM_ITEM.ENUM_ITEM_BISHOP,
			ENUM_ITEM.ENUM_ITEM_KING, ENUM_ITEM.ENUM_ITEM_QUEEN,
			ENUM_ITEM.ENUM_ITEM_BISHOP, ENUM_ITEM.ENUM_ITEM_KNIGHT,
			ENUM_ITEM.ENUM_ITEM_ROOK };

	public GameBoard() {
		Initialize();
		mGameSolver = new GameLogicAlphaBetaSearch();
	}
	
	public void Initialize() {
		ChessSquareDescriptor squareDescriptor;
		mGameSquare = new ChessSquareDescriptor[ChessGameConstants.FINAL_ROW_SIZE][ChessGameConstants.FINAL_COLUMN_SIZE];
		for( int i=0; i<ChessGameConstants.FINAL_COLUMN_SIZE; i++ ) {
			squareDescriptor = new ChessSquareDescriptor();
			squareDescriptor.mItem = mInitialItems[i];
			squareDescriptor.mPlayerType = ENUM_PLAYER.ENUM_PLAYER_BLACK;
			mGameSquare[0][i] = squareDescriptor;
		}
		for( int i=0; i<ChessGameConstants.FINAL_COLUMN_SIZE; i++ ) {
			squareDescriptor = new ChessSquareDescriptor();
			squareDescriptor.mItem = ENUM_ITEM.ENUM_ITEM_PAWN;
			squareDescriptor.mPlayerType = ENUM_PLAYER.ENUM_PLAYER_BLACK;
			mGameSquare[1][i] = squareDescriptor;
		}
		for (int i = 2; i < ChessGameConstants.FINAL_ROW_SIZE - 2; i++) {
			for (int j = 0; j < ChessGameConstants.FINAL_COLUMN_SIZE; j++) {
				squareDescriptor = new ChessSquareDescriptor();
				squareDescriptor.mItem = ENUM_ITEM.ENUM_ITEM_EMPTY;
				squareDescriptor.mPlayerType = ENUM_PLAYER.ENUM_PLAYER_NONE;
				mGameSquare[i][j] = squareDescriptor;
			}
		}
		for( int i=0; i<ChessGameConstants.FINAL_COLUMN_SIZE; i++ ) {
			squareDescriptor = new ChessSquareDescriptor();
			squareDescriptor.mItem = ENUM_ITEM.ENUM_ITEM_PAWN;
			squareDescriptor.mPlayerType = ENUM_PLAYER.ENUM_PLAYER_WHITE;
			mGameSquare[6][i] = squareDescriptor;
		}
		for( int i=0; i<ChessGameConstants.FINAL_COLUMN_SIZE; i++ ) {
			squareDescriptor = new ChessSquareDescriptor();
			squareDescriptor.mItem = mInitialItems[i];
			squareDescriptor.mPlayerType = ENUM_PLAYER.ENUM_PLAYER_WHITE;
			mGameSquare[7][i] = squareDescriptor;
		}
		
		//mTestEvaluateFunc = new SimpleEvaluateClass();
	}
	
	//PlayerTurn is taken care in the first steps itself
	public boolean IsLegalMove( int toPosX, int toPosY, int fromPosX, int fromPosY ) {
		if( toPosX == fromPosX && toPosY == fromPosY ) 
			return false;

		ChessSquareDescriptor desc;
		desc = mGameSquare[fromPosX][fromPosY];
		switch(desc.mItem) {
		case ENUM_ITEM_PAWN:
			 return IsPawnMovePossible(toPosX, toPosY, fromPosX, fromPosY, desc.mPlayerType );
		case ENUM_ITEM_ROOK:
			return IsRookMovePossible( toPosX, toPosY, fromPosX, fromPosY );
		case ENUM_ITEM_KNIGHT:
			return IsKnightMovePossible( toPosX, toPosY, fromPosX, fromPosY);
		case ENUM_ITEM_BISHOP:
			return IsBishopMovePossible( toPosX, toPosY, fromPosX, fromPosY );
		case ENUM_ITEM_QUEEN:
			return IsQueenMovePossible( toPosX, toPosY, fromPosX, fromPosY );
		case ENUM_ITEM_KING:
			return IsKingMovePossible( toPosX, toPosY, fromPosX, fromPosY );
			default:
				break;
		}
		return true;
	}
	
	public void TestEvaluateFunction() throws UnsupportedOperationException {
		//System.out.println("Evaluation value: " + mTestEvaluateFunc.Evaluate(mGameSquare));
		
		throw(new UnsupportedOperationException());
	}
	
	private boolean IsKingMovePossible(int toPosX, int toPosY, int fromPosX,
			int fromPosY) {
		// TODO Auto-generated method stub
		if (Math.abs(toPosX - fromPosX) > 1
				|| Math.abs(toPosX - fromPosX) > 1
				|| mGameSquare[toPosX][toPosY].mPlayerType == mGameSquare[fromPosX][fromPosY].mPlayerType)
			return false;
		
		//System.out.println("Level 0 reached!!!");
		return !IsPositionAttacked( toPosX, toPosY, mGameSquare[fromPosX][fromPosY].mPlayerType );
	}

	private boolean IsQueenMovePossible(int toPosX, int toPosY, int fromPosX,
			int fromPosY) {
		// TODO Auto-generated method stub
		return IsBishopMovePossible(toPosX, toPosY, fromPosX, fromPosY)
				|| IsRookMovePossible(toPosX, toPosY, fromPosX, fromPosY);
	}

	private boolean IsBishopMovePossible(int toPosX, int toPosY, int fromPosX,
			int fromPosY) {
		// TODO Auto-generated method stub
		if ((Math.abs(toPosX - fromPosX) != Math.abs(toPosY - fromPosY))
				|| mGameSquare[toPosX][toPosY].mPlayerType == mGameSquare[fromPosX][fromPosY].mPlayerType)
			return false;
		
		if( (fromPosX<toPosX) && (fromPosY<toPosY) ) {
			fromPosX++;
			fromPosY++;
			while( fromPosX < toPosX && fromPosY < toPosY ) {
				if( mGameSquare[fromPosX][fromPosY].mItem != ENUM_ITEM.ENUM_ITEM_EMPTY ) {
					return false;
				}
				fromPosX++;
				fromPosY++;
			}
		} else if( (fromPosX<toPosX)&&(fromPosY>toPosY)){
			fromPosX++;
			fromPosY--;
			while( fromPosX < toPosX && fromPosY < toPosY ) {
				if( mGameSquare[fromPosX][fromPosY].mItem != ENUM_ITEM.ENUM_ITEM_EMPTY ) {
					return false;
				}
				fromPosX++;
				fromPosY--;
			}
		} else if( (fromPosX>toPosX) && (fromPosY<toPosY)  ) {
			fromPosX--;
			fromPosY++;
			while( fromPosX > toPosX && fromPosY < toPosY ) {
				if( mGameSquare[fromPosX][fromPosY].mItem != ENUM_ITEM.ENUM_ITEM_EMPTY ) {
					return false;
				}
				fromPosX--;
				fromPosY++;
			}	
		} else {
			fromPosX--;
			fromPosY--;
			while( fromPosX > toPosX && fromPosY > toPosY ) {
				if( mGameSquare[fromPosX][fromPosY].mItem != ENUM_ITEM.ENUM_ITEM_EMPTY ) {
					return false;
				}
				fromPosX--;
				fromPosY--;
			}
		}
		
		return true;
	}

	private boolean IsKnightMovePossible(int toPosX, int toPosY, int fromPosX,
			int fromPosY) {
		if (!((Math.abs(toPosX - fromPosX) == 1 && Math.abs(toPosY - fromPosY) == 2) || (Math
				.abs(toPosX - fromPosX) == 2 && Math.abs(toPosY - fromPosY) == 1)))
			return false;
		if (mGameSquare[toPosX][toPosY].mPlayerType == mGameSquare[fromPosX][fromPosY].mPlayerType)
			return false;
		
		return true;
	}

	private boolean IsRookMovePossible(int toPosX, int toPosY, int fromPosX,
			int fromPosY) {
		// TODO Auto-generated method stub
		int start, end;
		if ((toPosX != fromPosX && toPosY != fromPosY)
				|| (mGameSquare[toPosX][toPosY].mPlayerType == mGameSquare[fromPosX][fromPosY].mPlayerType) )
			return false;
		if( toPosY == fromPosY ) {
			if( toPosX < fromPosX ) {
				start = toPosX;
				end = fromPosX;
			} else {
				start = fromPosX;
				end = toPosX;
			}
			for( int i=start+1; i<end; i++ ) {
				if( mGameSquare[i][toPosY].mItem != ENUM_ITEM.ENUM_ITEM_EMPTY )
					return false;
			}
		} else {
			if( toPosY < fromPosY ) {
				start = toPosY;
				end = fromPosY;
			} else {
				start = fromPosY;
				end = toPosY;
			}
			for( int i=start+1; i<end; i++ ) {
				if( mGameSquare[toPosX][i].mItem != ENUM_ITEM.ENUM_ITEM_EMPTY )
					return false;
			}	
		}
		return true;
	}

	//It won't check if valid move exists let it be taken care by IsLegalMove
	public boolean DoesLegalItemExists( int posX, int posY, ENUM_PLAYER playerTurn ) {
		
		ChessSquareDescriptor desc;
		
		desc = mGameSquare[posX][posY];
		
		if( playerTurn == ENUM_PLAYER.ENUM_PLAYER_NONE || desc.mPlayerType != playerTurn )
			return false;
		
		return true;
	}
	
	/*
	 * Rule for PRAWN move
	 * i. White always moves from higher row to lower row
	 * ii. Black always moves from lower row to higher row
	 * iii. White/Black can move two position from initial position ( TO BE DONE)
	 * iv. Black/White can move horizontally only when it can attack never otherwise
	 */
	private boolean IsPawnMovePossible( int toPosX, int toPosY, int fromPosX, int fromPosY, ENUM_PLAYER player ) {
		
		if( player == ENUM_PLAYER.ENUM_PLAYER_WHITE ) {
			if( (fromPosX-toPosX) != 1 )
				return false;
			if( fromPosY != toPosY ) {
				if( Math.abs(fromPosY-toPosY) != 1 )
					return false;
				if( mGameSquare[toPosX][toPosY].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
					return false;
				}
			}
		} else {
			if( (toPosX-fromPosX) != 1 )
				return false;
			if( fromPosY != toPosY ) {
				if( Math.abs(fromPosY-toPosY) != 1 )
					return false;
				if( mGameSquare[toPosX][toPosY].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_WHITE )
					return false;
			}
			
		}
		return true;
	}
	
	public static ENUM_PLAYER getOpponent(ENUM_PLAYER player) {
		if (player == ENUM_PLAYER.ENUM_PLAYER_BLACK)
			return ENUM_PLAYER.ENUM_PLAYER_WHITE;
		else if (player == ENUM_PLAYER.ENUM_PLAYER_WHITE) {
			return ENUM_PLAYER.ENUM_PLAYER_BLACK;
		}

		return ENUM_PLAYER.ENUM_PLAYER_NONE;
	}
	
	public boolean IsPositionAttacked(int posX, int posY, ENUM_PLAYER player) {

		// Check Right
		for (int i = posY + 1; i < ChessGameConstants.FINAL_COLUMN_SIZE; i++) {
			if (mGameSquare[posX][i].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[posX][i].mPlayerType != player ) {
					switch (mGameSquare[posX][i].mItem) {
					case ENUM_ITEM_KING:
						if ((i - posY) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_ROOK:
						return true;
					default:
						break;
					}
				}
				break;
			}
		}

		// Check Left
		for (int i = posY - 1; i >= 0; i--) {
			if (mGameSquare[posX][i].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[posX][i].mPlayerType == getOpponent(player)) {
					switch (mGameSquare[posX][i].mItem) {
					case ENUM_ITEM_KING:
						if ((posY - i) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_ROOK:
						return true;
					default:
						break;
					}
				}
				break;
			}
		}

		// Check Top
		for (int i = posX - 1; i >= 0; i--) {
			if (mGameSquare[i][posY].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[i][posY].mPlayerType != player) {
					switch (mGameSquare[i][posY].mItem) {
					case ENUM_ITEM_KING:
						if ((posX - i) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_ROOK:
						return true;
					default:
						break;
					}
				}
				break;
			}
		}

		// Check Bottom
		for (int i = posX + 1; i < ChessGameConstants.FINAL_ROW_SIZE; i++) {
			if (mGameSquare[i][posY].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[i][posY].mPlayerType != player) {
					switch (mGameSquare[i][posY].mItem) {
					case ENUM_ITEM_KING:
						if ((i - posX) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_ROOK:
						return true;
					default:
						break;
					}
				}
				break;
			}
		}

		// Check LeftTop
		for (int i = posX - 1, j = posY - 1; i >= 0 && j >= 0; i--, j--) {
			if (mGameSquare[i][j].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[i][j].mPlayerType != player ) {
					switch (mGameSquare[i][j].mItem) {
					case ENUM_ITEM_PAWN:
						// Needs to be Implemented
						if( player == ENUM_PLAYER.ENUM_PLAYER_BLACK ) 
							break;
					case ENUM_ITEM_KING:
						if ((posX - i) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_BISHOP:
						return true;
						default:
							break;
					}
				}
				break;
			}
		}

		// Check RightTop
		for (int i = posX - 1, j = posY + 1; i >= 0
				&& j < ChessGameConstants.FINAL_COLUMN_SIZE; i--, j++) {
			if (mGameSquare[i][j].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[i][j].mPlayerType != player) {
					switch (mGameSquare[i][j].mItem) {
					case ENUM_ITEM_PAWN:
						// Needs to be Implemented
						if( player == ENUM_PLAYER.ENUM_PLAYER_BLACK ) 
							break;
						break;
					case ENUM_ITEM_KING:
						if ((posX - i) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_BISHOP:
						return true;
						default:
							break;
					}
				}
				break;
			}
		}

		// Check LeftBottom
		for (int i = posX + 1, j = posY - 1; i < ChessGameConstants.FINAL_ROW_SIZE
				&& j >= 0; i++, j--) {
			if (mGameSquare[i][j].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[i][j].mPlayerType != player) {
					switch (mGameSquare[i][j].mItem) {
					case ENUM_ITEM_PAWN:
						// Needs to be Implemented
						if( player == ENUM_PLAYER.ENUM_PLAYER_WHITE ) 
							break;
						break;
					case ENUM_ITEM_KING:
						if ((posX - i) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_BISHOP:
						return true;
					default:
						break;
					}
				}
				break;
			}
		}

		// Check RightBottom
		for (int i = posX + 1, j = posY + 1; i < ChessGameConstants.FINAL_ROW_SIZE
				&& j < ChessGameConstants.FINAL_COLUMN_SIZE; i++, j++) {
			if (mGameSquare[i][j].mPlayerType != ENUM_PLAYER.ENUM_PLAYER_NONE) {
				if (mGameSquare[i][j].mPlayerType != player) {
					switch (mGameSquare[i][j].mItem) {
					case ENUM_ITEM_PAWN:
						// Needs to be Implemented
						if( player == ENUM_PLAYER.ENUM_PLAYER_WHITE ) 
							break;
						break;
					case ENUM_ITEM_KING:
						if ((posX - i) == 1)
							return true;
						break;
					case ENUM_ITEM_QUEEN:
					case ENUM_ITEM_BISHOP:
						return true;
					default:
						break;
					}
				}
				break;
			}
		}
		
		return false;
	}
	
	public boolean CheckIfCheckMate( ENUM_PLAYER player ) {
		for( int i=0; i<mGameSquare.length; i++ ) {
			for( int j=0; j<mGameSquare[0].length; j++ ){
				if( mGameSquare[i][j].mItem == ENUM_ITEM.ENUM_ITEM_KING && mGameSquare[i][j].mPlayerType == player )
					return IsPositionAttacked( i, j, player);
			}
		}
		
		return false;
	}
	
	public boolean IsGameOver( ENUM_PLAYER player ) {
		for( int i=0; i<mGameSquare.length; i++ ) {
			for( int j=0; j<mGameSquare[0].length; j++ ) {
				if( mGameSquare[i][j].mItem == ENUM_ITEM.ENUM_ITEM_KING && mGameSquare[i][j].mPlayerType == player )
					return false;
			}
		}
		
		return true;
	}
	
	public void Move( int toX, int toY, int fromX, int fromY ) {
		mGameSquare[toX][toY].mItem = mGameSquare[fromX][fromY].mItem;
		mGameSquare[toX][toY].mPlayerType = mGameSquare[fromX][fromY].mPlayerType;
		
		mGameSquare[fromX][fromY].mItem = ENUM_ITEM.ENUM_ITEM_EMPTY;
		mGameSquare[fromX][fromY].mPlayerType = ENUM_PLAYER.ENUM_PLAYER_NONE;
	}
	
	public NextMoveDesc FindNextMove() {
		//return ;
		
		return mGameSolver.FindNextMove(mGameSquare, ENUM_PLAYER.ENUM_PLAYER_BLACK, 4 );
	}
}
