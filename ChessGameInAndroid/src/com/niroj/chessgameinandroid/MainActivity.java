package com.niroj.chessgameinandroid;


import com.niroj.bluetooth.BluetoothService;
import com.niroj.bluetooth.Constants;
import com.niroj.bluetooth.DeviceListActivity;
import com.niroj.chessgameinandroid.CustomButton.ENUM_SQUARE_COLOR;
import com.niroj.chessgamelogic.GameBoard;
import com.niroj.chessgamelogic.AbstractGameSolver.NextMoveDesc;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_ITEM;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


public class MainActivity extends ActionBarActivity {
	
	private GridLayout mGameGrid;
    private volatile GameBoard mGameLogicBoard;
    private volatile ENUM_PLAYER mCurrentPlayer = ENUM_PLAYER.ENUM_PLAYER_NONE;
    private TextView mMessage;
    private Activity mActivity;
    private boolean mIsBluetoothSet = false;
    

    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;

    /**
     * Member object for the chat services
     */
    private BluetoothService mBluetoothService = null;

    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mActivity = this;

		mMessage = (TextView) findViewById(R.id.msgText);
		mGameGrid = (GridLayout) findViewById(R.id.gameGrid);
		mGameGrid.setRowCount(8);
		mGameGrid.setColumnCount(8);

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		//int height = size.y;

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				CustomButton customBtn;
				int index = i*8+j;
				if ((j % 2 == 1 && i % 2 == 1) || (j % 2 == 0 && i % 2 == 0)) {
					customBtn = new CustomButton(this,
							ENUM_SQUARE_COLOR.ENUM_SQUARE_COLOR_WHITE, index);
				} else {
					customBtn = new CustomButton(this,
							ENUM_SQUARE_COLOR.ENUM_SQUARE_COLOR_BLACK, index );
				}
				customBtn.setOnClickListener(mClickListener);
				mGameGrid.addView(customBtn, width / 9, width / 9);
			}
		}
		mGameLogicBoard = new GameBoard();
		Initialize();
		showMessage("", MESSAGE_TYPE_NORMAL);
		
	}
    
    @Override
    public void onStart() {
    	super.onStart();
		if (mIsBluetoothSet) {
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);

				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else if (mBluetoothService == null) {
				mBluetoothService = new BluetoothService(this, mHandler);
			}
		}
    }
    /**
     * Makes this device discoverable.
     */
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    private void setStatus(CharSequence subTitle) {
        final ActionBar actionBar = getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    private final Handler mHandler = new Handler() {
        private String mConnectedDeviceName;

		@Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            setStatus("Connected to: " + mConnectedDeviceName);
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            setStatus("Connecting");
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            setStatus("Not Connected");
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    break;
                case Constants.MESSAGE_READ:
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    Toast.makeText(mActivity, "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case Constants.MESSAGE_TOAST:
                        Toast.makeText(mActivity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    mBluetoothService = new BluetoothService(this, mHandler);
                } else {
                    // User did not enable Bluetooth or an error occurred
                   // Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Bluetooth is not enabled",
                            Toast.LENGTH_SHORT).show();
                }
        }
    }
    

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mBluetoothService.connect(device, secure);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater(); 
        inflater.inflate(R.menu.bluetooth, menu);
        
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.enableBluetooth: {
			mIsBluetoothSet = true;
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if( mBluetoothAdapter == null ) {
				Toast.makeText(this, "Bluetooth Device Not Present", Toast.LENGTH_SHORT).show();
				return true;
			}
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);

				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else if (mBluetoothService == null) {
				mBluetoothService = new BluetoothService(this, mHandler);
			}
			return true;
		}
		case R.id.secure_connect_scan: {
			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
			return true;
		}
		case R.id.insecure_connect_scan: {
			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent,
					REQUEST_CONNECT_DEVICE_INSECURE);
			return true;
		}
		case R.id.discoverable: {
			// Ensure this device is discoverable by others
			ensureDiscoverable();
			return true;
		}
		}
        return false;
    }
    
	private void showMessage(String msg, int msgType ) {
		// TODO Auto-generated method stub
		String str;
		
		switch( msgType ) {
		case MESSAGE_TYPE_NORMAL:
			mMessage.setTextColor(Color.GREEN);
			break;
		case MESSAGE_TYPE_WARNING:
			mMessage.setTextColor(Color.YELLOW);
			break;
		case MESSAGE_TYPE_ERROR:
			mMessage.setTextColor(Color.RED);
			break;
		}
		if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
			str = msg + "Black Player Move";
		} else if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_WHITE) {
			str = msg + "White Player Move";
		} else
			str = "Invalid state";
		mMessage.setText(str);
	}


	private void Initialize() {
		// TODO Auto-generated method stub
		InitializeBitmap();
		//Fill the board with items
		for( int i=0; i<8; i++ ) {
			for (int j = 0; j < 8; j++) {
				IView iView = (IView) mGameGrid.getChildAt(i * 8 + j);
				if (i == 0) {
					iView.SetItem(
							STARTING_ROW[j],
							ENUM_PLAYER.ENUM_PLAYER_BLACK,
							GetBitmap(STARTING_ROW[j],
									ENUM_PLAYER.ENUM_PLAYER_BLACK));
				} else if (i == 1) {
					iView.SetItem(
							ENUM_ITEM.ENUM_ITEM_PAWN,
							ENUM_PLAYER.ENUM_PLAYER_BLACK,
							GetBitmap(ENUM_ITEM.ENUM_ITEM_PAWN,
									ENUM_PLAYER.ENUM_PLAYER_BLACK));
				} else if( i == 6 ) {
					iView.SetItem(
							ENUM_ITEM.ENUM_ITEM_PAWN,
							ENUM_PLAYER.ENUM_PLAYER_WHITE,
							GetBitmap(ENUM_ITEM.ENUM_ITEM_PAWN,
							 		ENUM_PLAYER.ENUM_PLAYER_WHITE));
				} else if( i== 7) {
					iView.SetItem(
							STARTING_ROW[j],
							ENUM_PLAYER.ENUM_PLAYER_WHITE,
							GetBitmap(STARTING_ROW[j],
									ENUM_PLAYER.ENUM_PLAYER_WHITE));
				}
			}
		}
		
		mGameLogicBoard.Initialize();
		mCurrentPlayer = ENUM_PLAYER.ENUM_PLAYER_WHITE;
	}


	private OnClickListener mClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View view) {
			// TODO Auto-generated method stub
			
			if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK )
				return;
			
			IView currentView = (IView) view;
			int index = currentView.GetIndex();
			if( !mIsPrevPosSelected ) {
				int posX = index/8;
				int posY = index%8;
				if (mGameLogicBoard.DoesLegalItemExists(posX, posY,
						mCurrentPlayer)) {
					mIsPrevPosSelected = true;
					mPrevPosIndex = index;
					currentView.Notify(null, FINAL_INT_BUTTON_SELECT);
				}
			} else {
				mIsPrevPosSelected = false;
				IView prevView = (IView) mGameGrid.getChildAt(mPrevPosIndex);
				prevView.Notify(null, FINAL_INT_BUTTON_DESELECT);
				
				if( index == mPrevPosIndex )
					return; //Just deselecting the currently selected view
				int fromPosX, fromPosY, toPosX, toPosY;
				fromPosX = mPrevPosIndex/8;
				fromPosY = mPrevPosIndex%8;
				toPosX = index/8;
				toPosY = index%8;
				if( !mGameLogicBoard.IsLegalMove(toPosX, toPosY, fromPosX, fromPosY)) {
					showMessage("Wrong Move ", MESSAGE_TYPE_ERROR);
					
					return;
				}
				setNewLocation( prevView, currentView, toPosX, toPosY, fromPosX, fromPosY);
				
				if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
					new ComputerGameLogicTask().execute(null, null, null);
				}
			}
		}
	};
	

	
	private void setNewLocation( IView prevView, IView currentView, int toX, int toY, int fromX, int fromY ) {
		String str="";
		
		mGameLogicBoard.Move( toX, toY, fromX, fromY);
		ENUM_PLAYER player = prevView.GetPlayer();
		ENUM_ITEM item = prevView.GetItem();
		prevView.SetItem(ENUM_ITEM.ENUM_ITEM_EMPTY, ENUM_PLAYER.ENUM_PLAYER_NONE, null);
		currentView.SetItem(item, player, GetBitmap(item, player));
		
		mCurrentPlayer = GameBoard.getOpponent(mCurrentPlayer);
		if( mGameLogicBoard.CheckIfCheckMate(mCurrentPlayer) ) {
			Toast.makeText(this, "Check Mate", Toast.LENGTH_SHORT).show();
			str += "Check Mate "; 
		} else if( mGameLogicBoard.IsGameOver(mCurrentPlayer) ) {
			Toast.makeText(this, "Game Over", Toast.LENGTH_SHORT).show();
			str += "Game Over";
		}
		showMessage(str);
		
	}
	
	private class ComputerGameLogicTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_WHITE || mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_NONE )
				return null;
			final NextMoveDesc moveDesc = mGameLogicBoard.FindNextMove();
			final IView prevView = (IView)mGameGrid.getChildAt(moveDesc.mStartX*8+moveDesc.mStartY);
			final IView currentView = (IView) mGameGrid.getChildAt(moveDesc.mEndX*8+moveDesc.mEndY);
			mActivity.runOnUiThread(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					setNewLocation(prevView, currentView, moveDesc.mEndX, moveDesc.mEndY, moveDesc.mStartX, moveDesc.mStartY);
				}
				
			});
			// TODO Auto-generated method stub
			return null;
		}		
	}
	
	private void showMessage(String str) {
		if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
			str += "BLACK Player Turn";
		} else {
			str += "WHITE Player turn";
		}
		mMessage.setText(str);
	}
	
	
	private void InitializeBitmap() {
		mBitmapArray[FINAL_INT_BLACK][FINAL_INT_ROOK] = BitmapFactory.decodeResource(getResources(), R.drawable.rook_yellow);
		mBitmapArray[FINAL_INT_BLACK][FINAL_INT_KNIGHT] = BitmapFactory.decodeResource(getResources(), R.drawable.knight_yellow);
		mBitmapArray[FINAL_INT_BLACK][FINAL_INT_BISHOP] = BitmapFactory.decodeResource(getResources(), R.drawable.bishop_yellow);
		mBitmapArray[FINAL_INT_BLACK][FINAL_INT_QUEEN] = BitmapFactory.decodeResource(getResources(), R.drawable.queen_yellow);
		mBitmapArray[FINAL_INT_BLACK][FINAL_INT_KING] = BitmapFactory.decodeResource(getResources(), R.drawable.king_yellow);
		mBitmapArray[FINAL_INT_BLACK][FINAL_INT_PAWN] = BitmapFactory.decodeResource(getResources(), R.drawable.pawn_yellow);
		

		mBitmapArray[FINAL_INT_WHITE][FINAL_INT_ROOK] = BitmapFactory.decodeResource(getResources(), R.drawable.rook_white);
		mBitmapArray[FINAL_INT_WHITE][FINAL_INT_KNIGHT] = BitmapFactory.decodeResource(getResources(), R.drawable.knight_white);
		mBitmapArray[FINAL_INT_WHITE][FINAL_INT_BISHOP] = BitmapFactory.decodeResource(getResources(), R.drawable.bishop_white);
		mBitmapArray[FINAL_INT_WHITE][FINAL_INT_QUEEN] = BitmapFactory.decodeResource(getResources(), R.drawable.queen_white);
		mBitmapArray[FINAL_INT_WHITE][FINAL_INT_KING] = BitmapFactory.decodeResource(getResources(), R.drawable.king_white);
		mBitmapArray[FINAL_INT_WHITE][FINAL_INT_PAWN] = BitmapFactory.decodeResource(getResources(), R.drawable.pawn_white);
	}
	
	private Bitmap GetBitmap(ENUM_ITEM item, ENUM_PLAYER player ) {
		int col = -1, row = -1;
		switch(item) {
		case ENUM_ITEM_BISHOP:
			col = FINAL_INT_BISHOP;
			break;
		case ENUM_ITEM_EMPTY:
			return null;
		case ENUM_ITEM_KING:
			col = FINAL_INT_KING;
			break;
		case ENUM_ITEM_KNIGHT:
			col = FINAL_INT_KNIGHT;
			break;
		case ENUM_ITEM_PAWN:
			col = FINAL_INT_PAWN;
			break;
		case ENUM_ITEM_QUEEN:
			col = FINAL_INT_QUEEN;
			break;
		case ENUM_ITEM_ROOK:
			col = FINAL_INT_ROOK;
			break;
			default:
				return null;
		}
		
		switch(player) {
		case ENUM_PLAYER_BLACK:
			row = FINAL_INT_BLACK;
			break;
		case ENUM_PLAYER_NONE:
			return null;
		case ENUM_PLAYER_WHITE:
			row = FINAL_INT_WHITE;
			break;
			default:
				return null;
		}
		
		return mBitmapArray[row][col];
	}
	
	private boolean mIsPrevPosSelected = false;
	private int mPrevPosIndex = -1;
	
	public static final int FINAL_INT_BUTTON_SELECT = 0;
	public static final int FINAL_INT_BUTTON_DESELECT = 1;
	
	private Bitmap[][] mBitmapArray = new Bitmap[2][6];
	private final static int FINAL_INT_ROOK = 0;
	private final static int FINAL_INT_KNIGHT = 1;
	private final static int FINAL_INT_BISHOP = 2;
	private final static int FINAL_INT_QUEEN = 3;
	private final static int FINAL_INT_KING = 4;
	private final static int FINAL_INT_PAWN = 5;
	
	private final int FINAL_INT_BLACK = 0;
	private final int FINAL_INT_WHITE = 1;
	

    
    public static final int MESSAGE_TYPE_NORMAL = 0;
    public static final int MESSAGE_TYPE_WARNING = 1;
    public static final int MESSAGE_TYPE_ERROR = 2;

	public static final ENUM_ITEM[] STARTING_ROW = { ENUM_ITEM.ENUM_ITEM_ROOK,
			ENUM_ITEM.ENUM_ITEM_KNIGHT, ENUM_ITEM.ENUM_ITEM_BISHOP,
			ENUM_ITEM.ENUM_ITEM_KING, ENUM_ITEM.ENUM_ITEM_QUEEN,
			ENUM_ITEM.ENUM_ITEM_BISHOP, ENUM_ITEM.ENUM_ITEM_KNIGHT,
			ENUM_ITEM.ENUM_ITEM_ROOK };
	

	
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
}
