package com.niroj.chessgameinandroid;


import com.niroj.chessgamelogic.ChessGameConstants.ENUM_ITEM;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class CustomButton extends View implements IView {
	protected ENUM_SQUARE_COLOR mSquareColor;
	protected Bitmap mItemBitmap;
	protected ENUM_ITEM mItem;
	protected ENUM_PLAYER mPlayer;
	protected Paint mPaint;
	protected int mIndex;
	protected boolean mIsSelected = false;
	
	public CustomButton(Context context,
			ENUM_SQUARE_COLOR enumSquareColor, int index) {
		super(context);
		// TODO Auto-generated constructor stub
		mIndex = index;
		mSquareColor = enumSquareColor;
	}

	@Override
	public void onDraw( Canvas canvas ) {
		switch(mSquareColor) {
		case ENUM_SQUARE_COLOR_BLACK:
			canvas.drawARGB(255, 0, 0, 0);
			if( mIsSelected )
				canvas.drawARGB(100, 255, 0, 0);
			if( mItemBitmap != null ) 
				canvas.drawBitmap(mItemBitmap, 0, 0, mPaint);
			break;
		case ENUM_SQUARE_COLOR_WHITE:
			canvas.drawARGB( 255, 255, 255, 255);
			if( mIsSelected )
				canvas.drawARGB(100, 255, 0, 0);
			if( mItemBitmap != null ) 
				canvas.drawBitmap(mItemBitmap, 0, 0, mPaint);
			break;
			default:
				break;
		}
		
	}

	@Override
	public void OnProcess() {
		// TODO Auto-generated method stub
		
	}
	
	public enum ENUM_SQUARE_COLOR {
		ENUM_SQUARE_COLOR_BLACK,
		ENUM_SQUARE_COLOR_WHITE
	}

	@Override
	public int GetIndex() {
		// TODO Auto-generated method stub
		return mIndex;
	}

	@Override
	public void Notify( Object obj, int notifyNumber) {
		// TODO Auto-generated method stub
		switch(notifyNumber) {
		case MainActivity.FINAL_INT_BUTTON_SELECT:
			mIsSelected  = true;
			invalidate();
			break;
		case MainActivity.FINAL_INT_BUTTON_DESELECT:
			mIsSelected = false;
			invalidate();
			break;
		
		}
	}

	@Override
	public void SetItem(ENUM_ITEM item, ENUM_PLAYER player, Bitmap bmp) {
		// TODO Auto-generated method stub
		mItem = item;
		mItemBitmap = bmp;
		mPlayer = player;
		invalidate();
	}

	@Override
	public ENUM_ITEM GetItem() {
		// TODO Auto-generated method stub
		return mItem;
	}

	@Override
	public ENUM_PLAYER GetPlayer() {
		// TODO Auto-generated method stub
		return mPlayer;
	}
}
