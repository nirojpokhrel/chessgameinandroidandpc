package com.niroj.chessgameinandroid;

import com.niroj.chessgamelogic.ChessGameConstants.ENUM_ITEM;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;

import android.graphics.Bitmap;

public interface IView {
	public void OnProcess();
	public int GetIndex();
	public void Notify(Object obj, int notifyNumber);
	public void SetItem(ENUM_ITEM item, ENUM_PLAYER player, Bitmap bmp );
	public ENUM_ITEM GetItem();
	public ENUM_PLAYER GetPlayer();
}
