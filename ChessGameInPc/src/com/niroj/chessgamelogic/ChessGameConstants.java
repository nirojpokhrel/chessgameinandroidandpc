package com.niroj.chessgamelogic;

public class ChessGameConstants {

	
	public static enum ENUM_ITEM {
		ENUM_ITEM_EMPTY,
		ENUM_ITEM_QUEEN,
		ENUM_ITEM_KING,
		ENUM_ITEM_ROOK,
		ENUM_ITEM_KNIGHT,
		ENUM_ITEM_BISHOP,
		ENUM_ITEM_PAWN,
	};
	
	public static enum ENUM_PLAYER {
		ENUM_PLAYER_WHITE,
		ENUM_PLAYER_BLACK,
		ENUM_PLAYER_NONE
	};
	
	public static final int FINAL_ROW_SIZE = 8;
	public static final int FINAL_COLUMN_SIZE = 8;

	public static final int NEGATIVE_INFINITY = -1000000;
	public static final int POSITIVE_INFINITY = 1000000;
	
	public static final int CHESS_GAME_EASY_DEPTH = 10;
	public static final int CHESS_GAME_MEDIUM_DEPTH = 15;
	public static final int CHESS_GAME_DIFFICULT_DEPTH = 20;
}
