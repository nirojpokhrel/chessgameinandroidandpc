package com.niroj.chessgameinpc;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.swing.*;
import javax.swing.border.*;

import java.io.File;

import javax.imageio.ImageIO;

import com.niroj.chessgamelogic.AbstractGameSolver.NextMoveDesc;
import com.niroj.chessgamelogic.ChessGameConstants.ENUM_PLAYER;
import com.niroj.chessgamelogic.GameBoard;

public class ChessBoard3 {

	private static JFrame mJFrame;
	private ImageIcon mDefaultIcon;
    private final JPanel mGUI = new JPanel(new BorderLayout(3, 3));
    private JButton[][] mChessBoardSquares = new JButton[8][8];
    private Image[][] mChessPieceImages = new Image[2][6];
    private JPanel mChessBoard;
    private volatile GameBoard mGameLogicImpl;
    private volatile ENUM_PLAYER mCurrentPlayer = ENUM_PLAYER.ENUM_PLAYER_NONE;
    private final JLabel mGameMessage = new JLabel(
            "Niroj Chess Engine!!!");
    private static final String COLS = "ABCDEFGH";
    public static final int QUEEN = 0, KING = 1,
            ROOK = 2, KNIGHT = 3, BISHOP = 4, PAWN = 5;
    public static final int[] STARTING_ROW = {
        ROOK, KNIGHT, BISHOP, KING, QUEEN, BISHOP, KNIGHT, ROOK
    };
    public static final int BLACK = 0, WHITE = 1;

    ChessBoard3() {
        initializemGUI();
        mGameLogicImpl = new GameBoard();
       // mThread.start();
    }

    public final void initializemGUI() {
        // create the images for the chess pieces
        createImages();

        // set up the main mGUI
        mGUI.setBorder(new EmptyBorder(5, 5, 5, 5));
        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        mGUI.add(tools, BorderLayout.PAGE_START);
        Action newGameAction = new AbstractAction("New") {

            @Override
            public void actionPerformed(ActionEvent e) {
                setupNewGame();
                mCurrentPlayer = ENUM_PLAYER.ENUM_PLAYER_WHITE;
                mGameLogicImpl.Initialize();
            }
        };
        tools.add(newGameAction);
        tools.add(new JButton("Save")); // TODO - add functionality!
        tools.add(new JButton("Restore")); // TODO - add functionality!
        tools.addSeparator();
        tools.add(new JButton("Resign")); // TODO - add functionality!
        tools.addSeparator();
        tools.add(mGameMessage);

        mGUI.add(new JLabel("?"), BorderLayout.LINE_START);

        mChessBoard = new JPanel(new GridLayout(0, 9)) {

            /**
             * Override the preferred size to return the largest it can, in
             * a square shape.  Must (must, must) be added to a GridBagLayout
             * as the only component (it uses the parent as a mGUIde to size)
             * with no GridBagConstaint (so it is centered).
             */
            @Override
            public final Dimension getPreferredSize() {
                Dimension d = super.getPreferredSize();
                Dimension prefSize = null;
                Component c = getParent();
                if (c == null) {
                    prefSize = new Dimension(
                            (int)d.getWidth(),(int)d.getHeight());
                } else if (c!=null &&
                        c.getWidth()>d.getWidth() &&
                        c.getHeight()>d.getHeight()) {
                    prefSize = c.getSize();
                } else {
                    prefSize = d;
                }
                int w = (int) prefSize.getWidth();
                int h = (int) prefSize.getHeight();
                // the smaller of the two sizes
                int s = (w>h ? h : w);
                return new Dimension(s,s);
            }
        };
        mChessBoard.setBorder(new CompoundBorder(
                new EmptyBorder(8,8,8,8),
                new LineBorder(Color.BLACK)
                ));
        
        // Set the BG to be ochre
        Color bgColor = new Color(204,119,34);
        mChessBoard.setBackground(bgColor);
        JPanel boardConstrain = new JPanel(new GridBagLayout());
        boardConstrain.setBackground(bgColor);
        boardConstrain.add(mChessBoard);
        mGUI.add(boardConstrain);

        mDefaultIcon = new ImageIcon(new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB));
        // create the chess board squares
        Insets buttonMargin = new Insets(0, 0, 0, 0);
        for (int ii = 0; ii < mChessBoardSquares.length; ii++) {
            for (int jj = 0; jj < mChessBoardSquares[ii].length; jj++) {
                JButton b = new JButton(""+(ii*mChessBoardSquares[ii].length+jj));
                b.setMargin(buttonMargin);
                // our chess pieces are 64x64 px in size, so we'll
                // 'fill this in' using a transparent icon..
//                ImageIcon icon = new ImageIcon(
//                        );
                b.setIcon(mDefaultIcon);
                b.addActionListener(mActionListener);
                if ((jj % 2 == 1 && ii % 2 == 1)
                        //) {
                        || (jj % 2 == 0 && ii % 2 == 0)) {
                    b.setBackground(Color.WHITE);
                } else {
                    b.setBackground(Color.BLACK);
                }
                mChessBoardSquares[ii][jj] = b;
            }
        }

        /*
         * fill the chess board
         */
        mChessBoard.add(new JLabel(""));
        // fill the top row
        for (int ii = 0; ii < 8; ii++) {
            mChessBoard.add(
                    new JLabel(COLS.substring(ii, ii + 1),
                    SwingConstants.CENTER));
        }
        // fill the black non-pawn piece row
        for (int ii = 0; ii < 8; ii++) {
            for (int jj = 0; jj < 8; jj++) {
                switch (jj) {
                    case 0:
                        mChessBoard.add(new JLabel("" + (9-(jj + 1)),
                                SwingConstants.CENTER));
                    default:
                        mChessBoard.add(mChessBoardSquares[ii][jj]);
                }
            }
        }
    }
    
    ActionListener mActionListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			
			if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK )
				return;
			
			String strPos = event.getActionCommand();
			
			int pos = Integer.parseInt(strPos);
			
			int i = pos / mChessBoardSquares.length;
			int j = pos % mChessBoardSquares.length;
			if( mItemSelected == true ) {
				if( mPrevPos.mX == i && mPrevPos.mY == j ) {
					mItemSelected = false;
					
					return;
				}
				if( !mGameLogicImpl.IsLegalMove(i,j, mPrevPos.mX, mPrevPos.mY ) ) {
					//mGameMessage.setText("Illegal Move");
					showMessage("Illegal Move ");
					mItemSelected = false;
					
					return;
				}
				setNewLocation( i, j, mPrevPos.mX, mPrevPos.mY );
	            mItemSelected = false;
				if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
					new CustomThread().start();
				}
			} else {
				if (mGameLogicImpl.DoesLegalItemExists(i, j, mCurrentPlayer)) {
					mPrevPos.mX = i;
					mPrevPos.mY = j;
					mItemSelected = true;
				}
			}
			
		}
	};
	
	private Runnable mLogicThread = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			NextMoveDesc moveDesc = mGameLogicImpl.FindNextMove();
			setNewLocation(moveDesc.mEndX, moveDesc.mEndY, moveDesc.mStartX, moveDesc.mStartY);
		}
		
	};
	
	private class CustomThread extends Thread {
		@Override
		public void run() {
			if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_WHITE || mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_NONE )
				return;
			NextMoveDesc moveDesc = mGameLogicImpl.FindNextMove();
			setNewLocation(moveDesc.mEndX, moveDesc.mEndY, moveDesc.mStartX, moveDesc.mStartY);
		}
	};
	
	private void setNewLocation( int toX, int toY, int fromX, int fromY ) {
		mGameLogicImpl.Move( toX, toY, fromX, fromY);
        mChessBoardSquares[toX][toY].setIcon(mChessBoardSquares[fromX][fromY].getIcon());
        if ((fromX % 2 == 1 && fromY % 2 == 1)
                //) {
                || (fromY % 2 == 0 && fromX % 2 == 0)) {
        	mChessBoardSquares[fromX][fromY].setIcon(mDefaultIcon);
        	mChessBoardSquares[fromX][fromY].setBackground(Color.WHITE);
        } else {
        	mChessBoardSquares[fromX][fromY].setIcon(mDefaultIcon);
        	mChessBoardSquares[fromX][fromY].setBackground(Color.BLACK);
        }
        mItemSelected = false;
		mCurrentPlayer = GameBoard.getOpponent(mCurrentPlayer);
		if( mGameLogicImpl.CheckIfCheckMate(mCurrentPlayer) ) {
			JOptionPane.showMessageDialog(mJFrame,
			    "Check Mate",
			    "Check Mate",
			    JOptionPane.PLAIN_MESSAGE);
			
		} else if( mGameLogicImpl.IsGameOver(mCurrentPlayer) ) {
			JOptionPane.showMessageDialog(mJFrame,
				    "You Lose!!!",
				    "Game Over",
				    JOptionPane.PLAIN_MESSAGE);}
		showMessage("");
		
	}
	
	private void showMessage(String str) {
		if( mCurrentPlayer == ENUM_PLAYER.ENUM_PLAYER_BLACK ) {
			str += "BLACK Player Turn";
		} else {
			str += "WHITE Player turn";
		}
		mGameMessage.setText(str);
		
		
	}

    public final JComponent getmGUI() {
        return mGUI;
    }

    private final void createImages() {
        try {
            BufferedImage bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\queen_yellow.png"));
            mChessPieceImages[BLACK][0] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\king_yellow.png"));
            mChessPieceImages[BLACK][1] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\rook_yellow.png"));
            mChessPieceImages[BLACK][2] = bi;


            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\knight_yellow.png"));
            mChessPieceImages[BLACK][3] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\bishop_yellow.png"));
            mChessPieceImages[BLACK][4] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\pawn_yellow.png"));
            mChessPieceImages[BLACK][5] = bi;
            

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\queen_white.png"));
            mChessPieceImages[WHITE][0] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\king_white.png"));
            mChessPieceImages[WHITE][1] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\rook_white.png"));
            mChessPieceImages[WHITE][2] = bi;


            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\knight_white.png"));
            mChessPieceImages[WHITE][3] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\bishop_white.png"));
            mChessPieceImages[WHITE][4] = bi;

            bi = ImageIO.read(new File("E:\\ChessGameInAndroidAndPc\\ChessGameInPc\\res\\pawn_white.png"));
            mChessPieceImages[WHITE][5] = bi;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Initializes the icons of the initial chess board piece places
     */
    private final void setupNewGame() {
    	mGameMessage.setText("Make your move!");
        // set up the black pieces
        for (int ii = 0; ii < STARTING_ROW.length; ii++) {
            mChessBoardSquares[0][ii].setIcon(new ImageIcon(new ImageIcon(
                    mChessPieceImages[BLACK][STARTING_ROW[ii]]).getImage().getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)));
        }
        for (int ii = 0; ii < STARTING_ROW.length; ii++) {
            mChessBoardSquares[1][ii].setIcon(new ImageIcon(new ImageIcon(
                    mChessPieceImages[BLACK][PAWN]).getImage().getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)));
        }
        // set up the white pieces
        for (int ii = 0; ii < STARTING_ROW.length; ii++) {
            mChessBoardSquares[6][ii].setIcon(new ImageIcon(new ImageIcon(
                    mChessPieceImages[WHITE][PAWN]).getImage().getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)));
        }
        for (int ii = 0; ii < STARTING_ROW.length; ii++) {
            mChessBoardSquares[7][ii].setIcon(new ImageIcon(new ImageIcon(
                    mChessPieceImages[WHITE][STARTING_ROW[ii]]).getImage().getScaledInstance(64, 64, java.awt.Image.SCALE_SMOOTH)));
        }
    }

    public static void main(String[] args) {
        Runnable r = new Runnable() {

            @Override
            public void run() {
            	ChessBoard3 cg = new ChessBoard3();

                JFrame f = new JFrame("ChessChamp");
                mJFrame = f;
                f.add(cg.getmGUI());
                // Ensures JVM closes after frame(s) closed and
                // all non-daemon threads are finished
                f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                // See http://stackoverflow.com/a/7143398/418556 for demo.
                f.setLocationByPlatform(true);

                // ensures the frame is the minimum size it needs to be
                // in order display the components within it
                f.pack();
                // ensures the minimum size is enforced.
                f.setMinimumSize(f.getSize());
                f.setVisible(true);
            }
        };
        // Swing mGUIs should be created and updated on the EDT
        // http://docs.oracle.com/javase/tutorial/uiswing/concurrency
        SwingUtilities.invokeLater(r);
    }
    
    private class Position {
    	public int mX;
    	public int mY;
    }
    
    private Position mPrevPos = new Position();
    private boolean mItemSelected = false;
}
